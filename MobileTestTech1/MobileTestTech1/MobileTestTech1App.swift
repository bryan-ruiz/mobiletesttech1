//
//  MobileTestTech1App.swift
//  MobileTestTech1
//
//  Created by FF_BryanR on 6/8/22.
//

import SwiftUI

@main
struct MobileTestTech1App: App {
    let persistenceController = PersistenceController.shared
    @StateObject var serviceDB: ServiceDB = ServiceDB()
    
    var body: some Scene {
        WindowGroup {
            ContentView()
                .environment(\.managedObjectContext, persistenceController.container.viewContext)
                .environmentObject(serviceDB)
        }
    }
}
