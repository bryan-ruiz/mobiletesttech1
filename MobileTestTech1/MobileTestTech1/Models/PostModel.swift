//
//  PostModel.swift
//  MobileTestTech1
//
//  Created by FF_BryanR on 6/12/22.
//

import Foundation

struct PostModel: Decodable, Identifiable {
    let id: Int
    let userId: Int
    let title: String
    let body: String
}
