//
//  CompanyModel.swift
//  MobileTestTech1
//
//  Created by FF_BryanR on 6/12/22.
//

import Foundation

struct CompanyModel: Decodable {
    let name: String
    let catchPhrase: String
    let bs: String
    
    enum CodingKeys: String, CodingKey {
        case name = "name"
        case catchPhrase = "catchPhrase"
        case bs = "bs"
    }
}
