//
//  GeoModel.swift
//  MobileTestTech1
//
//  Created by FF_BryanR on 6/12/22.
//

import Foundation

struct GeoModel: Decodable {
    let lat: String
    let lng: String
    
    enum CodingKeys: String, CodingKey {
        case lat = "lat"
        case lng = "lng"
    }
}
