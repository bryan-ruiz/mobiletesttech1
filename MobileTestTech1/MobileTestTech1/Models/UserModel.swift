//
//  UserModel.swift
//  MobileTestTech1
//
//  Created by FF_BryanR on 6/12/22.
//

import Foundation

public struct UserModel: Decodable, Identifiable {
    public let id: Int
    let email: String
    let name: String
    let phone: String
    let website: String
    let username: String
    let address: AddressModel
    let company: CompanyModel
}
