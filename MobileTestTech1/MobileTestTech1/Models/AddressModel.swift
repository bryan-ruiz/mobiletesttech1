//
//  AddressModel.swift
//  MobileTestTech1
//
//  Created by FF_BryanR on 6/12/22.
//

import Foundation

struct AddressModel: Decodable {
    let street: String
    let suite: String
    let city: String
    let zipcode: String
    let geo: GeoModel
    
    enum CodingKeys: String, CodingKey {
        case street = "street"
        case suite = "suite"
        case city = "city"
        case zipcode = "zipcode"
        case geo = "geo"
    }
}
