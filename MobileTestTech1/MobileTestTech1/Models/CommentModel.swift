//
//  CommentModel.swift
//  MobileTestTech1
//
//  Created by FF_BryanR on 6/12/22.
//

import Foundation

public struct CommentModel: Decodable, Identifiable {
    public let id: Int
    let postId: Int
    let email: String
    let body: String
}
