//
//  ContentView.swift
//  MobileTestTech1
//
//  Created by FF_BryanR on 6/8/22.
//

import SwiftUI
import CoreData
import Combine

struct ContentView: View {
    var body: some View {
        NavigationView {
            PostListView()
        }
    }
}
