//
//  ServiceAPI.swift
//  MobileTestTech1
//
//  Created by FF_BryanR on 6/12/22.
//

import Combine
import Foundation
import UIKit

enum ErrorsAPI: Error {
    case EmptyPosts
    case EmptyUsers
    case EmptyComments
    case InvalidUrl
}

public struct ServiceAPI {
    func getUsersFrom(post: String) async -> Result<UserModel, Error>? {
        do {
            let url = URL(string: "https://jsonplaceholder.typicode.com/users/\(post)")
            
            guard let url = url else {
                return .failure(ErrorsAPI.InvalidUrl)
            }
            
            let (data, _) = try await URLSession.shared.data(from: url)
            let user = try JSONDecoder().decode(UserModel.self, from: data)
            return .success(user)
            
        } catch(let error) {
            return .failure(error)
        }
    }
    
    func getPostList() async -> Result<[PostModel], Error>? {
        do {
            let url = URL(string: "https://jsonplaceholder.typicode.com/posts")
            
            guard let url = url else {
                return .failure(ErrorsAPI.InvalidUrl)
            }
            
            let (data, _) = try await URLSession.shared.data(from: url)
            let postList = try JSONDecoder().decode([PostModel].self, from: data)
            return .success(postList)
            
        } catch(let error) {
            return .failure(error)
        }
    }
    
    func getCommentListFrom(post: String) async -> Result<[CommentModel], Error>? {
        do {
            let url = URL(string: "https://jsonplaceholder.typicode.com/posts/\(post)/comments")
            
            guard let url = url else {
                return .failure(ErrorsAPI.InvalidUrl)
            }
            
            let (data, _) = try await URLSession.shared.data(from: url)
            let commentList = try JSONDecoder().decode([CommentModel].self, from: data)
            return .success(commentList)
            
        } catch(let error) {
            return .failure(error)
        }
    }
}
