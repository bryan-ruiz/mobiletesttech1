//
//  PostListCell.swift
//  MobileTestTech1
//
//  Created by FF_BryanR on 6/12/22.
//

import SwiftUI

struct PostListCell: View {
    @ObservedObject var postListCellViewModel: StandardPostListCellViewModel
    
    init(post: PostEntity) {
        self.postListCellViewModel = StandardPostListCellViewModel(post: post)
    }
    
    var body: some View {
        NavigationLink(destination: PostDetailView(post: postListCellViewModel.post), label: {
            HStack {
                if postListCellViewModel.post.isFavorite {
                    Image(systemName: "star.fill")
                        .foregroundColor(.yellow)
                }
                
                Text(postListCellViewModel.post.title ?? L10n.empty)
            }
        })
    }
}
