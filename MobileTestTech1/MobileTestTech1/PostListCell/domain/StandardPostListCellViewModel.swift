//
//  StandardPostListCellViewModel.swift
//  MobileTestTech1
//
//  Created by FF_BryanR on 6/13/22.
//

import Foundation

final class StandardPostListCellViewModel: ObservableObject, PostListCellViewModel {

    @Published var post: PostEntity
    @Published var isFavorite: Bool

    init(post: PostEntity) {
        self.post = post
        isFavorite = false
    }
}
