//
//  PostListCellViewModel.swift
//  MobileTestTech1
//
//  Created by FF_BryanR on 6/10/22.
//

import Foundation

public protocol PostListCellViewModel: ObservableObject {
    var post: PostEntity { get }
    var isFavorite: Bool { get }
}
