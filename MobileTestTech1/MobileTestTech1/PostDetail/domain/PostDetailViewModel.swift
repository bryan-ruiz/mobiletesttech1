//
//  PostDetailViewModel.swift
//  MobileTestTech1
//
//  Created by FF_BryanR on 6/10/22.
//

import Foundation

public protocol PostDetailViewModel {
    var userModel: UserModel? { get }
    var commentList: [CommentModel] { get }
    var serviceAPI: ServiceAPI { get }
    var post: PostEntity { get }

    func fetchUser()
    func fetchCommentList()
}
