//
//  StandardPostDetailViewModel.swift
//  MobileTestTech1
//
//  Created by FF_BryanR on 6/13/22.
//

import Foundation

class StandardPostDetailViewModel: ObservableObject {
    @Published var userModel: UserModel?
    @Published var commentList: [CommentModel]
    private let serviceAPI: ServiceAPI
    
    var post: PostEntity

    init(post: PostEntity) {
        self.post = post
        self.commentList = []
        self.serviceAPI = ServiceAPI()
        self.userModel = nil
    }
    
    func fetchUser() {
        Task(priority: .background) {
            let result: Result<UserModel, Error>? = await serviceAPI.getUsersFrom(post: "\(post.id ?? 0)")
            guard let result = result else { return }
            guard case .success(let user) = result else { return }
            
            DispatchQueue.main.async {
                self.userModel = user
            }
        }
    }
    
    func fetchCommentList() {
        Task(priority: .background) {
            let result: Result<[CommentModel], Error>? = await serviceAPI.getCommentListFrom(post: "\(post.id ?? 0)")
            guard let result = result else { return }
            guard case .success(let commentList) = result else { return }
            
            DispatchQueue.main.async {
                self.commentList = commentList
            }
        }
    }
}
