//
//  PostDetailView.swift
//  MobileTestTech1
//
//  Created by FF_BryanR on 6/12/22.
//

import SwiftUI

struct PostDetailView: View {
    @ObservedObject var postDetailViewModel: StandardPostDetailViewModel
    @EnvironmentObject var serviceDB: ServiceDB
    @Environment(\.managedObjectContext) private var viewContext
    @Environment(\.presentationMode) var presentationMode
    
    init(post: PostEntity) {
        self.postDetailViewModel = StandardPostDetailViewModel(post: post)
    }
    
    var body: some View {
        VStack(alignment: .leading, spacing: 15) {
            HStack {
                Text(L10n.description)
                    .bold()
                    .font(.title)
            }
            .padding(.horizontal, 20)
            .padding(.top, 10)
            
            Text(postDetailViewModel.post.body ?? L10n.empty)
                .padding(.horizontal, 20)
            
            Text(L10n.user)
                .bold()
                .font(.title)
                .padding(.horizontal, 20)
            
            Text("\(L10n.name)\(L10n.colon) \(postDetailViewModel.userModel?.name ?? L10n.empty)")
                    .padding(.horizontal, 20)
            
            Text("\(L10n.email)\(L10n.colon) \(postDetailViewModel.userModel?.email ?? L10n.empty)")
                .padding(.horizontal, 20)
            
            Text("\(L10n.phone)\(L10n.colon) \(postDetailViewModel.userModel?.phone ?? L10n.empty)")
                .padding(.horizontal, 20)
            
            Text("\(L10n.website)\(L10n.colon) \(postDetailViewModel.userModel?.website ?? L10n.empty)")
                .padding(.horizontal, 20)
                        
            Text("\(L10n.comments)")
                .font(.title)
                .bold()
                .textCase(.uppercase)
                .padding(.horizontal, 20)
                .frame(maxWidth: .infinity, alignment: .leading)
                .background(.bar)
            
            List(postDetailViewModel.commentList) { comment in
                CommentCell(comment: comment)
                    .padding(.horizontal, 20)
            }
            .listStyle(.inset)
            
            Spacer()
        }
        .onAppear {
            postDetailViewModel.fetchUser()
            postDetailViewModel.fetchCommentList()
        }
        .toolbar {
            ToolbarItem(placement: .navigationBarTrailing) {
                HStack {
                    Button(action: {
                        presentationMode.wrappedValue.dismiss()
                        serviceDB.delete(context: viewContext, postModel: postDetailViewModel.post)
                    }, label: {
                        Image(uiImage: UIImage(named: Asset.deleteImage.name)!)
                            .resizable()
                            .frame(width: 25, height: 25, alignment: .center)
                            .scaledToFit()
                    })
                    
                    Button(action: {
                        presentationMode.wrappedValue.dismiss()
                        serviceDB.isFavorite(post: postDetailViewModel.post, context: viewContext, isManualAction: true)
                    }, label: {
                        Image(uiImage: UIImage(named: Asset.starImage.name)!)
                            .resizable()
                            .frame(width: 35, height: 35, alignment: .center)
                            .scaledToFit()
                    })
                }
            }
            
        }
        .navigationBarTitle(postDetailViewModel.post.title ?? L10n.empty, displayMode: .inline)
    }
}
