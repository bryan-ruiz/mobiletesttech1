// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

import Foundation

// swiftlint:disable superfluous_disable_command file_length implicit_return

// MARK: - Strings

// swiftlint:disable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:disable nesting type_body_length type_name vertical_whitespace_opening_braces
internal enum L10n {
  /// All
  internal static let all = L10n.tr("Localizable", "all")
  /// :
  internal static let colon = L10n.tr("Localizable", "colon")
  /// Comments
  internal static let comments = L10n.tr("Localizable", "comments")
  /// Delete All
  internal static let deleteAll = L10n.tr("Localizable", "deleteAll")
  /// Description
  internal static let description = L10n.tr("Localizable", "description")
  /// Email
  internal static let email = L10n.tr("Localizable", "email")
  /// 
  internal static let empty = L10n.tr("Localizable", "empty")
  /// Favorites
  internal static let favorites = L10n.tr("Localizable", "favorites")
  /// id
  internal static let id = L10n.tr("Localizable", "id")
  /// Name
  internal static let name = L10n.tr("Localizable", "name")
  /// Phone
  internal static let phone = L10n.tr("Localizable", "phone")
  /// Posts
  internal static let posts = L10n.tr("Localizable", "posts")
  /// shouldLoadData
  internal static let shouldLoadData = L10n.tr("Localizable", "shouldLoadData")
  /// User
  internal static let user = L10n.tr("Localizable", "user")
  /// Website
  internal static let website = L10n.tr("Localizable", "website")
}
// swiftlint:enable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:enable nesting type_body_length type_name vertical_whitespace_opening_braces

// MARK: - Implementation Details

extension L10n {
  private static func tr(_ table: String, _ key: String, _ args: CVarArg...) -> String {
    let format = BundleToken.bundle.localizedString(forKey: key, value: nil, table: table)
    return String(format: format, locale: Locale.current, arguments: args)
  }
}

// swiftlint:disable convenience_type
private final class BundleToken {
  static let bundle: Bundle = {
    #if SWIFT_PACKAGE
    return Bundle.module
    #else
    return Bundle(for: BundleToken.self)
    #endif
  }()
}
// swiftlint:enable convenience_type
