//
//  StandardCommentCellViewModel.swift
//  MobileTestTech1
//
//  Created by FF_BryanR on 6/13/22.
//

import Foundation

final class StandardCommentCellViewModel: ObservableObject, CommentCellViewModel {
    @Published var comment: CommentModel

    init(comment: CommentModel) {
        self.comment = comment
    }
}
