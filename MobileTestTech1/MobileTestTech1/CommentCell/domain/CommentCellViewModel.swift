//
//  CommentCellViewModel.swift
//  MobileTestTech1
//
//  Created by FF_BryanR on 6/13/22.
//

import Foundation

public protocol CommentCellViewModel: ObservableObject {
    var comment: CommentModel { get }
}
