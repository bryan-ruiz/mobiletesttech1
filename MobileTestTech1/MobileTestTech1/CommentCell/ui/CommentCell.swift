//
//  CommentCell.swift
//  MobileTestTech1
//
//  Created by FF_BryanR on 6/13/22.
//

import SwiftUI

struct CommentCell: View {
    @ObservedObject var commentCellViewModel: StandardCommentCellViewModel
    
    init(comment: CommentModel) {
        self.commentCellViewModel = StandardCommentCellViewModel(comment: comment)
    }
    
    var body: some View {
        Text(commentCellViewModel.comment.body)
    }
}
