//
//  ServiceDB.swift
//  MobileTestTech1
//
//  Created by FF_BryanR on 6/13/22.
//


import Foundation
import CoreData
import Combine

public class ServiceDB: ObservableObject {
    func save(context: NSManagedObjectContext) {
        do {
            try context.save()
        }
        catch {
            print(error)
        }
    }
    
    func createPost(context: NSManagedObjectContext, postModel: PostModel) {
        let post = PostEntity(context: context)
        post.id = postModel.id.toDecimal()
        post.title = postModel.title
        post.body = postModel.body
        post.userId = postModel.userId.toDecimal()
        post.isFavorite = false
        save(context: context)
    }
    
    func isFavorite(post: PostEntity, context: NSManagedObjectContext, isManualAction: Bool = false) {
        if isManualAction {
            post.isFavorite.toggle()
        } else {
            post.isFavorite = true
        }
        save(context: context)
    }
    
    func delete(context: NSManagedObjectContext, postModel: PostEntity) {
        context.delete(postModel)
        save(context: context)
    }
    
    func deleteAllData(context: NSManagedObjectContext, entity: String) {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
        fetchRequest.returnsObjectsAsFaults = false
        do {
            let results = try context.fetch(fetchRequest)
            for object in results {
                guard let objectData = object as? NSManagedObject else {continue}
                context.delete(objectData)
            }
        } catch let error {
            print("Detele all data in \(entity) error :", error)
        }
    }
}

extension Int {
    func toDecimal() -> NSDecimalNumber {
        return NSDecimalNumber(value: self)
    }
}
