//
//  StandardPostListViewModel.swift
//  MobileTestTech1
//
//  Created by FF_BryanR on 6/13/22.
//

import Foundation
import CoreData

final class StandardPostListViewModel: ObservableObject, PostListViewModel {
    public var favoriteList: [NSDecimalNumber?]
    public let serviceAPI: ServiceAPI
    public var serviceDB: ServiceDB?
    public var viewContext: NSManagedObjectContext?
    public var isRefreshing: Bool
    
    public init() {
        favoriteList = []
        serviceAPI = ServiceAPI()
        isRefreshing = true
    }
    
    func fetchPostList() {
        if UserDefaults.standard.value(forKey: L10n.shouldLoadData) == nil {
            Task(priority: .background) {
                let result: Result<[PostModel], Error>? = await serviceAPI.getPostList()
                guard let result = result else { return }
                guard case .success(let postList) = result else { return }
                
                DispatchQueue.main.async {
                    guard let viewContext = self.viewContext else { return }
                    guard let serviceDB = self.serviceDB else { return }
                    serviceDB.deleteAllData(context: viewContext, entity: PostEntity.description())
                    self.isRefreshing = true
                    postList.forEach { post in
                        guard let serviceDB = self.serviceDB else { return }
                        serviceDB.createPost(context: viewContext, postModel: post)
                        UserDefaults.standard.set(false, forKey: L10n.shouldLoadData)
                        
                    }
                }
            }
        }
    }
    
    func getList(favoriteList: [PostEntity], normalList: [PostEntity], isFavorite: ListState) -> [PostEntity] {
        
        if !favoriteList.isEmpty {
            favoriteList.forEach { favorite in
                if !self.favoriteList.contains(favorite.id) {
                    self.favoriteList.append(favorite.id)
                }
            }
        }
        
        var favoriteListToReturn: [PostEntity] = favoriteList
        var normalListToReturn: [PostEntity] = normalList
        
        if isRefreshing {
            self.favoriteList.forEach { favorite in
                let filtered: [PostEntity]? = normalList.filter { $0.id == favorite }
                
                guard let filteredFavorite = filtered?.first else { return }
                guard let viewContext = self.viewContext else { return }
                
                favoriteListToReturn.append(filteredFavorite)
                
                guard let itemToRemove = filtered?.first else { return }
                if let index = normalList.firstIndex(of: itemToRemove) {
                    normalListToReturn.remove(at: index)
                }
                
                self.serviceDB?.isFavorite(post: filteredFavorite, context: viewContext)
                isRefreshing = false
            }
        } else {
            
        }
        
        if isFavorite == .Favorites {
            return favoriteListToReturn
        } else {
            return favoriteListToReturn + normalListToReturn
        }
    }
    
    func setServiceDB(serviceDB: ServiceDB) {
        self.serviceDB = serviceDB
    }
    
    func setViewContext(viewContext: NSManagedObjectContext) {
        self.viewContext = viewContext
    }
    
    func resetFavorites() {
        favoriteList = []
    }
}
