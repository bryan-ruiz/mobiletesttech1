//
//  PostListViewModel.swift
//  MobileTestTech1
//
//  Created by FF_BryanR on 6/10/22.
//

import Foundation
import CoreData

public protocol PostListViewModel: ObservableObject {
    var favoriteList: [NSDecimalNumber?] { get }
    var serviceAPI: ServiceAPI { get }
    var serviceDB: ServiceDB? { get }
    var viewContext: NSManagedObjectContext? { get }
    var isRefreshing: Bool { get }
    
    func fetchPostList()
    func getList(favoriteList: [PostEntity], normalList: [PostEntity], isFavorite: ListState) -> [PostEntity]
    func setServiceDB(serviceDB: ServiceDB)
    func setViewContext(viewContext: NSManagedObjectContext)
    func resetFavorites()
}
