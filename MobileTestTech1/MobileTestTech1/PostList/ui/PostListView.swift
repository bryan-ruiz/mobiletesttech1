//
//  PostListView.swift
//  MobileTestTech1
//
//  Created by FF_BryanR on 6/12/22.
//

import SwiftUI

public enum ListState: String {
    case All
    case Favorites
}

struct PostListView: View {
    @Environment(\.managedObjectContext) private var viewContext
    @ObservedObject var postListViewModel: StandardPostListViewModel = StandardPostListViewModel()
    @State private var isFavoriteList: ListState = ListState.All
    @EnvironmentObject var serviceDB: ServiceDB
    @FetchRequest(
        entity: PostEntity.entity(),
        sortDescriptors: [NSSortDescriptor(key: L10n.id, ascending: true)])
    private var postList: FetchedResults<PostEntity>
    
    
    var body: some View {
        VStack {
            HStack {
                Button(action: {
                    isFavoriteList = ListState.All
                }, label: {
                    Text(L10n.all)
                        .frame(width: 150, height: 12, alignment: .center)
                        .font(.headline)
                        .foregroundColor(isFavoriteList == ListState.All ? .white : .green)
                        .padding()
                        .background(isFavoriteList == ListState.All ? .green : .white)
                        .cornerRadius(10)
                })
                
                Button(action: {
                    isFavoriteList = ListState.Favorites
                }, label: {
                    Text(L10n.favorites)
                        .frame(width: 150, height: 12, alignment: .center)
                        .font(.headline)
                        .foregroundColor(isFavoriteList == ListState.Favorites ? .white : .green)
                        .padding()
                        .background(isFavoriteList == ListState.Favorites ? .green : .white)
                        .cornerRadius(10)
                })
            }
            .overlay(
                RoundedRectangle(cornerRadius: 10)
                    .stroke(.green, lineWidth: 2)
            )
                        
            List(postListViewModel.getList(favoriteList: postList.filter {$0.isFavorite}, normalList: postList.filter {!$0.isFavorite}, isFavorite: isFavoriteList)) { post in
                PostListCell(post: post)
            }
            .listStyle(.inset)
            .refreshable {
                UserDefaults.standard.removeObject(forKey: L10n.shouldLoadData)
                postListViewModel.fetchPostList()
            }
            
            Button(action: {
                postListViewModel.resetFavorites()
                serviceDB.deleteAllData(context: viewContext, entity: PostEntity.description())
            }, label: {
                Text(L10n.deleteAll)
                    .frame(maxWidth: .infinity, alignment: .center)
                    .font(.headline)
                    .foregroundColor(.white)
                    .padding()
                    .background(.red)
                    .cornerRadius(10)
            })
            .padding(20)
        }
        .onAppear {
            postListViewModel.setViewContext(viewContext: viewContext)
            postListViewModel.setServiceDB(serviceDB: serviceDB)
            postListViewModel.fetchPostList()
        }
        .navigationBarTitle(L10n.posts, displayMode: .inline)
        .navigationTitle(L10n.posts)
    }
}

struct PostListView_Previews: PreviewProvider {
    static var previews: some View {
        PostListView()
    }
}
